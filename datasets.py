import os
import pandas
import numpy as np


_this_directory = os.path.dirname(__file__)


def average(row, column_vectors):
    """Function which averages word vectors for words contained in a document.
    Does not weight fields"""
    vec = np.zeros(len(column_vectors[0]))

    words = 0

    for i, v in enumerate(row):
        if v != 0:
            words += 1
            vec += column_vectors[i]

    return vec / words


class Dataset(object):
    def __init__(self, filename):
        self._filename = filename

        self.index = int(os.path.basename(filename).split("_")[0])
        self.name = " ".join(
            os.path.basename(filename).split(".")[0].split("_")[1:])

        self._data = None

        self.identifier = self.index

    @property
    def properties(self):
        data = self.load()
        return dict(
            samples=data.size,
            features=len(data.columns) - 1 # All columns except "document"
        )

    def release(self):
        self._data = None

    def load(self):
        if self._data is None:
            self._data = pandas.read_csv(self._filename, sep=";")

        return self._data

    def labels(self, numeric=True):
        """Get labels from loaded dataset. These labels may be either numeric,
        or a string value.

        :param numeric: If the returned label should be a str or a number
        which is the same for objects with the same label.
        :type numeric: bool

        :rtype: [str] or [int]"""
        documents = self.load()['document']
        cats = set()
        l = []

        for doc in documents:
            cat = doc.split(".")[0]

            cats.add(cat)

            l.append(cat)

        if numeric:
            cats_dict = dict(zip(cats, range(0, len(cats))))

            return [cats_dict[d] for d in l]

        return l

    def ft_vectors(self, model, document_vector_strategy=average):
        """Load vectors according to the model.

        :param model: Model used to extract vectors, intended to be a FastText
        model.
        :param document_vector_strategy: Callable used to convert samples from
        BoW to document vectors. The first argument is the BoW sample, a matrix
        in that each line is a word vector for the correspoding column text.

        :returns: A vector matrix, each line is the document vector for the
        correspoding sample."""
        sample = []

        data = self.load()

        column_vectors = [model[c] for c in data.columns if c != 'document']

        for row in data.values:
            sample.append(document_vector_strategy(row[1:], column_vectors))

        return np.array(sample)

    """Method deprecated in favour of ft_vectors (FastText Vectors)"""
    load_vectors = ft_vectors


class Datasets(object):
    def __init__(self):
        dataset_dir = os.path.join(_this_directory, "dsets")
        datasets = [os.path.join(dataset_dir, dset)
                    for dset in os.listdir(dataset_dir)
                    if not dset.startswith(".")]

        self.dsets = dict()

        for dset in datasets:
            d = Dataset(dset)
            self.dsets[d.index] = d

    def __getitem__(self, index):
        return self.dsets[index]

    def __iter__(self):
        return iter(self.dsets.values())
