import logging
import collections
from collections import defaultdict
from sklearn.cluster import AgglomerativeClustering
from sklearn.metrics import adjusted_rand_score, normalized_mutual_info_score
from sklearn.neighbors import KDTree, DistanceMetric
from sklearn import preprocessing
import reporter
import numpy as np
import scipy.spatial.distance as d
from annoy import AnnoyIndex
import fasttext
from gensim.test.utils import datapath
import datasets
import copy


logger = logging.getLogger(__name__)


Expandable = NewType('Expandable', Iterable[Any])

def g(x):
    return Expandable(x)

config = {
    "repeat": 3,
    "neighbours": g(range(3, 14, 2)),
    "linkage": g(["ward", "average"]),
    "kdtree_leaf_size": g(range(2, 41, 5)),
    "annoy_trees": g(range(10, 51, 5)),
    "search_k": -1,
    "algorithm": g(["simple", "kdtree", "annoy"]),
}

def expand_config(config: dict) -> Iterator[dict]:
    from collections import abc
    generators = []

    for k, v in config.items():
        if isinstance(v, ConfigGenerator):
            generators.append([(k, g) for g in v])

    for items in itertools.product(*generators):
        for item in items:
            config[item[0]] = item[1]

        yield config.copy()


class Evaluation(object):
    def __init__(self):
        self.reporter = None
        self.distance_fn = {}
        self.model = None
        self.cluster_parameters = dict()
        self.repeat = 2

    def register_distance(self, name):
        def wrap(fn):
            self.distance_fn[name] = fn
            return fn

        return wrap

    def evaluate(self, datasets):
        for dataset in datasets:
            ctx = self.reporter.in_dataset(dataset)

            self._eval_dataset(ctx, dataset)

    def _eval_dataset(self, ctx, dataset):
        dset_vectors = dataset.ft_vectors(self.model)

        for name, fn in self.distance_fn.items():
            with ctx.algorithm(name):
                for i in range(self.repeat):
                    self._cluster_with_func(dataset, dset_vectors, ctx, fn)

    def _cluster_with_func(self, dataset, vs, ctx, fn):
        params = self.cluster_parameters.copy()
        params["affinity"] = "precomputed"
        params["n_clusters"] = len(set(dataset.labels()))

        print(params)

        agg = AgglomerativeClustering(**params)

        with ctx.time_it():
            matrix = fn(vs)

            agg.fit(matrix)

        metric_args = (dataset.labels(), agg.labels_)
        ari = adjusted_rand_score(*metric_args)
        nmi = normalized_mutual_info_score(*metric_args)

        ctx.store_result((ari, nmi))


evaluation = Evaluation()


def _make_matrix(data, size, func):
    matrix = np.zeros((size, size))

    for i in range(0, size):
        for j in range(i + 1, size):
            matrix[i, j] = func(i, j, data[i], data[j])
            matrix[j, i] = matrix[i, j]

    return matrix


def _matrix_from_neighbours(data, size, neighbours):
    matrix = np.zeros((size, size))
    #inf = np.finfo(matrix.dtype).max
    inf = 2.0

    for i in range(0, size):
        ns = neighbours(i, data[i])

        for j in range(i + 1, size):
            matrix[i, j] = matrix[j, i] = (ns[j] if j in ns else inf)

    return matrix

@evaluation.register_distance("cosine")
def cosine(data):
    return _make_matrix(data, len(data), lambda i, j, x, y: 1 - d.cosine(x, y))

def _make_annoy_neighbour_dict(annoy):
    def wrapper(i, d):
        ns = annoy.get_nns_by_item(i, 5, include_distances=True)
        return dict(zip(*ns))

    return wrapper

def _make_kdtree_neighbour_dict(tree):
    def wrapper(i, d):
        d = np.array([d])

        d, i = tree.query(preprocessing.normalize(d, axis=1, norm='l2'), k=5)
        return dict(zip(i[0], d[0]))

    return wrapper

@evaluation.register_distance("annoy")
def annoy(data):
    index = AnnoyIndex(len(data[0]), "angular")

    for i in range(len(data)):
        index.add_item(i, data[i])

    index.build(10) # Build 10 trees

    return _matrix_from_neighbours(data, len(data), _make_annoy_neighbour_dict(index))

@evaluation.register_distance("kdtree")
def kdtree(data):
    t = KDTree(preprocessing.normalize(data, axis=1, norm='l2'))

    return _matrix_from_neighbours(data, len(data), _make_kdtree_neighbour_dict(t))


if __name__ == "__main__":
    cluster_params = dict(n_clusters=7, linkage="average")
    evaluation.model = fasttext.load_model(datapath("crime-and-punishment.bin"))
    evaluation.reporter = reporter.Reporter(cluster_params)
    evaluation.cluster_parameters = cluster_params

    dsets = datasets.Datasets()
    dsets = [dsets[i] for i in range(1)]

    evaluation.evaluate(dsets)
    evaluation.reporter.store("results")
