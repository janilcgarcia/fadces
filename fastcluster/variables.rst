===
Variables
===

When running the algorithms there will be some variables, some we need to set
only once, while a bunch of others will have be executed within a range. Here
they will be describe.

General Variables
===

repeat
  Number of runs for each of the variable combination

neighbours
  Number of neighbours to take when doing the clustering

n_clusters
  Number of clusters for the k-means

linkage
  Linkage strategy for AgglomerativeClustering


k-d tree
===

leaf_size
  Number of points to switch to brute-force as described in KDTree_.

Annoy Variables
===

n_trees
  Number of tree

.. _KDTree: https://scikit-learn.org/stable/modules/generated/sklearn.neighbors.KDTree.html
