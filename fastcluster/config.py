import collections
import collections.abc
import itertools


class Variant:
    """Indicates the values can be expanded to generate different configurations
    """

    def __init__(self, values):
        self.values = values

    def __iter__(self):
        return iter(self.values)

    def map(self, func):
        """Apply a transformation for every element in the values contained
        by this class

        :param func: Function to apply to all values
        :returns: MappedVariant that applies the function to all values"""
        return MappedVariant(func, self.values)


class MappedVariant:
    """A Variant that transform it's values before yielding them"""
    def __init__(self, func, values):
        self.func = func
        self.values = values

    def __iter__(self):
        for v in iter(self.values):
            yield self.func(v)


vary = Variant


def variate_dict(d):
    iterables = []
    iterators = []

    base = {}

    for k, v in d.items():
        if isinstance(v, Variant):
            iterables.append([(k, value) for value in v])
            iterators.append(variant_iter(iterables[-1]))

        else:
            base[k] = v

    if not iterators:
        yield d
        return

    gendict = {}

    for it in iterators:
        i = next(it)
        gendict[i[0]] = i[1]

    yield dict(collections.ChainMap(gendict.copy(), base).items())

    while True:
        i = next(iterators[-1], None)

        if i is None:
            idx = len(iterators) - 1
            i2 = None

            while idx > 0:
                iterators[idx] = variant_iter(iterables[idx])
                i2 = next(iterators[idx - 1], None)

                if i2 is not None:
                    gendict[i2[0]] = i2[1]
                    break
                else:
                    idx -= 1

            if idx == 0 and i2 is None:
                break

        else:
            gendict[i[0]] = i[1]

            yield dict(collections.ChainMap(gendict.copy(), base).items())


def variant_iter(it):
    for k, v in it:
        if isinstance(v, dict):
            for d in variate_dict(v):
                yield (k, d)

        else:
            yield (k, v)
