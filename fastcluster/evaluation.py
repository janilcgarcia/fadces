import logging
import traceback
import collections
import time
from contextlib import contextmanager
import os
import multiprocessing as mp
import functools
import concurrent

from collections import defaultdict

from sklearn.cluster import AgglomerativeClustering
from sklearn.metrics import adjusted_rand_score, normalized_mutual_info_score
from sklearn.neighbors import KDTree, DistanceMetric
from sklearn import preprocessing

import numpy as np
import scipy.spatial.distance as d

from annoy import AnnoyIndex

import fasttext
from gensim.test.utils import datapath

from . import datasets, config


logger = logging.getLogger(__name__)


class Timer:
    """High precision timer, used to time the execution of the clustering.
    Also keeps a `time_dict` with times for all time made blocks.
    This dictionary can be accessed directly or as an item or attribute
    of this object itself."""
    def __init__(self):
        self.time_dict = {}

    @contextmanager
    def start(self, name):
        """"Start a named timer. It's a context manager. Automatically
        adds a new entry after done. Calling this function twice with
        the same name will overwrite that entry.
        
        :param name: Name for the timed code
        :type name: str"""
        start = time.perf_counter()
        yield
        end = time.perf_counter()
        self.time_dict[name] = end - start


    def __getitem__(self, key):
        if key not in self.time_dict:
            return 0.0

        return self.time_dict[key]

    def __getattr__(self, key):
        return self[key]


def _make_matrix(data, size, func):
    """Create a distance matrix. For each index/data piece pair invoke func.
    But for the upper triangle, just mirrors lower triangle."""
    matrix = np.zeros((size, size))

    for i in range(0, size):
        for j in range(i + 1, size):
            matrix[i, j] = func(i, j, data[i], data[j])
            matrix[j, i] = matrix[i, j]

    return matrix


def _matrix_from_neighbours(data, size, neighbours):
    """Create a matrix using a list of neighbours. Instead of calling a
    function to calculate the distance between to samples, get the
    closest neighbours and their distances and add to the matrix. If
    a sample is not a neighbour, it's distance is set to infinity.
    """
    matrix = np.zeros((size, size))
    inf = 2.0

    for i in range(0, size):
        ns = neighbours(i, data[i])

        for j in range(i + 1, size):
            matrix[i, j] = matrix[j, i] = ns[j] if j in ns else inf

    return matrix


def cosine(conf, data, timer):
    with timer.start("matrix_build"):
        return _make_matrix(data, len(data),
                            lambda i, j, x, y: 1 - d.cosine(x, y))


def _make_annoy_neighbour_dict(conf, annoy):
    """Creates a list from neighbours using a annoy index"""
    def wrapper(i, d):
        ns = annoy.get_nns_by_item(
            i, conf["neighbours"],
            include_distances=True,
            search_k=conf["algo"]["search_k"])
        return dict(zip(*ns))

    return wrapper


def _make_kdtree_neighbour_dict(conf, tree):
    """Creates a list of neighbours using a k-d tree index"""
    def wrapper(i, d):
        d = np.array([d])

        d, i = tree.query(preprocessing.normalize(d, axis=1, norm='l2'),
                          k=conf["neighbours"])
        return dict(zip(i[0], d[0]))

    return wrapper


def annoy(conf, data, timer):
    with timer.start("indexing"):
        index = AnnoyIndex(len(data[0]), "angular")

        for i in range(len(data)):
            index.add_item(i, data[i])

        index.build(conf["algo"]["trees"])

    with timer.start("matrix_build"):
        return _matrix_from_neighbours(data, len(data),
                                       _make_annoy_neighbour_dict(conf, index))


def kdtree(conf, data, timer):
    with timer.start("indexing"):
        t = KDTree(preprocessing.normalize(data, axis=1, norm='l2'),
                   leaf_size=conf["algo"]["leaf_size"])

    with timer.start("matrix_build"):
        return _matrix_from_neighbours(data, len(data),
                                       _make_kdtree_neighbour_dict(conf, t))


_ALGOS = {
    "cosine-distance": cosine,
    "kdtree": kdtree,
    "annoy": annoy,
}


def eval_all(model: fasttext.FastText, config_generator: dict, datasets: list):
    """Evaluate all datasets with the generated configurations in parallel.

    :param model: FastText model for calculating document vectors

    :param config_generator: A configuration dict using config.vary fields
    to express configurations that must vary during testing

    :param datasets: All datasets for which to eval the results"""


    pool =  mp.pool.Pool(
        max(int(mp.cpu_count() * 0.8), 1))

    manager = mp.Manager()
    
    for dataset in datasets:
        # Load dataset vectors that will be reused in with every configuration.
        # That way eval_dataset don't have to re-calculate vectors each
        # different configuration
        name = dataset.name.replace("_bag", "")

        vs = manager.list(dataset.ft_vectors(model))
        logger.debug("Loaded vectors for %s", name)
        
        labels = manager.list(dataset.labels())
        logger.debug("Loaded labels for %s", name)

        confs = list(config.variate_dict(config_generator))
        confs = confs * confs[0]["repeat"]

        logger.info("Starting evaluation in %s", name)
        yield from pool.imap_unordered(
            functools.partial(
                eval_dataset, name=dataset.name, labels=labels, dset_vectors=vs
            ),
            confs
        )



def eval_dataset(conf, name, labels, dset_vectors):
    """Evalulate the dataset using the passed config

    :params conf: Configuration being evaluated
    """
    algo_function = _ALGOS[conf["algo"]["name"]]

    logger.info("evaluating dataset: %s, with algo: %s",
                name, conf["algo"]["name"])
    try:
        timer = Timer()
        agg = AgglomerativeClustering(affinity="precomputed",
                                  n_clusters=len(set(labels)),
                                  linkage=conf["linkage"])

        matrix = algo_function(conf, dset_vectors, timer)

        with timer.start("clustering"):
            agg.fit(matrix)

            # Calculate metrics for the clustering
            metric_args = (labels, agg.labels_)
            ari = adjusted_rand_score(*metric_args)
            nmi = normalized_mutual_info_score(*metric_args)

            # Create a new row for the results output. It contains all
            # configuration information, plus the result information
            row = {
                "conf": conf,
                "dataset": name,
                "exec_time": timer.time_dict.copy(),
                "ari": ari,
                "nmi": nmi,
            }
            logger.info("finished evaluating dataset %s/%s",
                        name, conf["algo"]["name"])

            return row

    except Exception as ex:
        logger.error("Exception details: %s", traceback.format_exc())



def test_run():
    import json
    logging.basicConfig(level=logging.DEBUG,
                        format=("%(levelname)s:%(filename)s:%(asctime)s"
                                ":%(processName)s:%(message)s"))

    conf = {
        "repeat": 3,
        "neighbours": 5,
        "linkage": "average",
        "algo": config.vary([{
            "name": "cosine-distance",
        }, {
            "name": "kdtree",
            "leaf_size": 10,
        }, {
            "name": "annoy",
            "search_k": -1,
            "trees": 10,
        }][1:]),
    }

    model = fasttext.load_model(datapath('crime-and-punishment.bin'))

    logger.info("Model loaded")

    ds = datasets.Datasets()
    ds = [ds[1]]

    for result in eval_all(model, conf, ds):
        if result:
            print(json.dumps(dict(result.items())))


def real_run():
    import json
    
    logging.basicConfig(level=logging.DEBUG,
                        format=("%(levelname)s:%(filename)s:%(asctime)s"
                                ":%(processName)s:%(message)s"))

    conf = {
        "repeat": 3,
        "neighbours": config.vary([5, 15, 30, 50]),
        "linkage": "average",
        "algo": config.vary([{
            "name": "cosine-distance",
        }, {
            "name": "kdtree",
            "leaf_size": 10,
        }, {
            "name": "annoy",
            "search_k": -1,
            "trees": 10,
        }][1:]),
    }

    model = fasttext.load_model('cc.en.300.bin')
    logger.info("Model loaded")

    ds = datasets.Datasets()

    with open('results.json', 'w') as f:
        for idx, result in enumerate(eval_all(model, conf, ds)):
            if result:
                json.dump(dict(result.items()), f)
                f.write("\n")
                f.flush()
                logger.info("Logged result %d", idx + 1)
            else:
                logger.warning("Failed to obtain result %d", idx + 1)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("--test", action="store_true")
    result = parser.parse_args()

    if result.test:
        test_run()
    else:
        real_run()
