from collections import defaultdict

import datetime as dt
import os
import os.path as path
import json
from contextlib import contextmanager
import time


class DatasetReporterContext(object):
    def __init__(self, dataset):
        self._properties = dataset.properties
        self._results = defaultdict(lambda: {
            "time": [],
            "results": [],
        })

        self._algo = None

    @contextmanager
    def algorithm(self, algo):
        self._algo = self._results[algo]
        yield
        self._algo = None

    def _check_algo(self):
        if self._algo is None:
            raise RuntimeError("An algorithm must be defined before timing it's"
                               " execution")

    @contextmanager
    def time_it(self):
        self._check_algo()

        t0 = time.perf_counter()
        yield
        tf = time.perf_counter()

        self._algo["time"].append(tf - t0)

    def store_result(self, results):
        self._check_algo()

        self._algo["results"].append(dict(
            ari=results[0],
            nmi=results[1],
        ))

    def to_dict(self):
        return dict(self._results)


class Reporter(object):
    def __init__(self, cluster_parameters):
        self._cluster_parameters = cluster_parameters

        self._datasets = {}

    def in_dataset(self, dataset):
        dset_ctx = DatasetReporterContext(dataset)
        self._datasets[dataset.identifier] = dset_ctx

        return dset_ctx

    def store(self, directory):
        if not path.isdir(directory):
            os.makedirs(directory)

        file_name = "report-" + str(int(dt.datetime.now().timestamp())) + ".json"

        file_name = path.join(directory, file_name)

        with open(file_name, "w") as f:
            json.dump(self._make_report(), f)

    def _make_report(self):
        d = dict(
            cluster_parameters=self._cluster_parameters,
        )

        for key, ctx in self._datasets.items():
            d[key] = ctx.to_dict()

        return d
