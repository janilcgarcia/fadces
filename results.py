from collections import defaultdict
import json
import time

from contextlib import contextmanager


class ResultReporter(object):
    """Reporter for clustering evaluation results"""
    def __init__(self):
        def make_dict():
            return {
                "exc_time": [],
                "metrics": {
                    "ari": 0.0,
                    "nmi": 0.0,
                },
            }

        self.algorithms = ["traditional", "annoy", "kdtree"]

        def make_defaultdict():
            d = {}

            for algo in self.algorithms:
                d[algo] = make_dict()

            return d

        self.report = {
            "results": defaultdict(make_defaultdict),
        }

        self.results = self.report["results"]

    def algo_parameters(self, params):
        self.report["params"] = params

    @contextmanager
    def add_timing(self, dataset, algo):
        """Add the time for the execution for a certain algorithm

        :param dataset: Dataset name
        :type dataset: str
        :param algo: Algorithm name (traditional, annoy, kdtree)
        :type dataset: str
        :param exc_time: Execution time with the clock available
        :type exc_time: float"""

        if algo not in self.algorithms:
            raise ValueError("{} algorithm is not valid for this aggregator")

        init_time = time.perf_counter()
        yield
        final_time = time.perf_counter()
        self.results[dataset][algo]["exc_time"].append(final_time - init_time)

    def add_results(self, dataset, algo, ari, nmi):
        """Add an evaluation result for the clustering with the specified
        algorithm."""
        if algo not in self.algorithms:
            raise ValueError("{} algorithm is not valid for this aggregator")

        self.results[dataset][algo]["metrics"] = {
            "ari": ari,
            "nmi": nmi,
        }

    def save(self, file_name):
        if not file_name.endswith(".json"):
            file_name = file_name + ".json"

        with open(file_name, "w") as f:
            json.dump(self.results, f)

